FROM openjdk:11.0.3-slim
COPY target/sample-0.0.1-SNAPSHOT.jar /tmp/sample-0.0.1-SNAPSHOT.jar
CMD [ "java", "-jar", "/tmp/sample-0.0.1-SNAPSHOT.jar", "--server.servlet.context-path=/sample" ,"&" ]

