//Config for target environments
config = [
  "development": [
    "target_environment": "dev"
  ],
  "release": [
    "target_environment": "qa"
  ],
  "master": [
    "target_environment": "prod"
  ]
]

//Returns branch name
def getBranchParentDir() {
    rawBranch = env.BRANCH_NAME
    startIndex = rawBranch.indexOf('/')

    if(startIndex == -1){
      return rawBranch
    }

    return rawBranch.substring(0,startIndex)
}

//To get the configuration values based on branch name
def getConfigValue(name) {
    configHash = config[getBranchParentDir()]

    if (configHash == null) {
        return ""
    }

    return configHash[name]
}

//To get the target environment from the config array
def getBuildTargetEnvironment() {
    environment = getConfigValue("target_environment")

    if (environment == null || environment == "") {
        return "dev"
    }

    return environment
}

//Delete docker image in the jenkins server.
def dockerCleanUp() {
    sh 'docker rmi -f ${repoURL}/${imageName}:latest'
}

//Declarative pipeline starts
pipeline {
  agent any

//Added environment variables
  environment{
    repoURL = "419991610827.dkr.ecr.us-east-1.amazonaws.com"
    imageName = "samplepy-${getBuildTargetEnvironment()}"
  }
  
  stages {
//Removing unused docker images
    stage('Removing unused images') {
      when {
            anyOf {
                branch "development"
                branch "release/*"
                branch "master"
           }
      }
      steps {
        sh 'docker image prune -a -f'
      }
    }
//Building of Jar and checking for package vulnerabilities with CVE and CWE databases
    stage('Building Jar & Dependency Check Analyze and Track') {
      when {
            anyOf {
                branch "development"
                branch "release/*"
                branch "master"
           }
      }

      steps {
        script {
          echo "${getBranchParentDir()}"
        }

        sh 'mvn clean install -DskipTests'
      }

    }
// Building the docker image
    stage('Docker Build') {
      when {
            anyOf {
                branch "development"
                branch "release/*"
                branch "master"
           }
      }
      steps {
        sh 'docker build -t ${repoURL}/${imageName} .'
      }
    }
// Scanning the docker image for vulnerabilities with AquaMicroScanner
    stage('Scanning of Image') {
      when {
            anyOf {
                branch "development"
                branch "release/*"
                branch "master"
           }
      }
      steps {
        aquaMicroscanner imageName: "${repoURL}/${imageName}",notCompliesCmd: 'exit 4',onDisallowed: 'fail',outputFormat: 'html'
      }
    }
// Docker login to AWS ECR and pushing image to ECR
    stage('Docker Login and Push to ECR') {
      when {
            anyOf {
                branch "development"
                branch "release/*"
                branch "master"
           }
      }
      environment {
        AWS_ACCESS_KEY_ID     = credentials('AWS_ACCESS_KEY_ID')
        AWS_SECRET_ACCESS_KEY = credentials('AWS_SECRET_ACCESS_KEY')
        REGION = credentials('ECR_REGION')
      }
      steps {
        sh '$(aws ecr get-login --no-include-email --region us-east-1)'
        sh 'docker push ${repoURL}/${imageName}:latest'
      }
    }
// Deleting all untagged images in ECR other than latest
    stage('Deleting all untagged images in ECR') {
      when {
            anyOf {
                branch "development"
                branch "release/*"
                branch "master"
           }
      }
      environment {
        AWS_ACCESS_KEY_ID     = credentials('AWS_ACCESS_KEY_ID')
        AWS_SECRET_ACCESS_KEY = credentials('AWS_SECRET_ACCESS_KEY')
        REGION = credentials('ECR_REGION')
      }
      steps {
        sh 'aws ecr list-images --region ${REGION} --repository-name ${imageName} --filter tagStatus=UNTAGGED --query "imageIds[*]" --output text | while read line; do aws ecr batch-delete-image --region ${REGION}  --repository-name ${imageName} --image-ids imageDigest=$line; done' 
      }
    }
// Removing local images within the server
    stage('Removing images') {
      when {
            anyOf {
                branch "development"
                branch "release/*"
                branch "master"
           }
      }
      steps {
        dockerCleanUp()
      }
    }

  }
// Archiving artifacts, Sending mail through SES and publishing vulnerability report to OWASP Dependency Track  
  post {
        always {
            archiveArtifacts artifacts: 'styles.css,scanlatest.html', onlyIfSuccessful: true
            dependencyTrackPublisher artifact: 'target/bom.xml', artifactType: 'bom', projectId: '0aa1960c-f58c-418f-8ee5-bb0133c9e485', synchronous: true
            
            emailext attachmentsPattern:'**/*.html,**/styles.css,target/bom.xml' ,attachLog: true, body: "${currentBuild.currentResult}: Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}",
                recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']],
                subject: "Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}"
        }
    }
}
